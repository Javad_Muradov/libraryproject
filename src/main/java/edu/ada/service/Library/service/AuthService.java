package edu.ada.service.Library.service;

import edu.ada.service.Library.model.dto.RegistrationModel;
import edu.ada.service.Library.model.entity.UserEntity;

public interface AuthService {

    UserEntity registration (RegistrationModel registrationModel);

    UserEntity login(String email, String password);
}

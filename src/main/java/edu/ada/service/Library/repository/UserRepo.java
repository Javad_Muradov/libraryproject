package edu.ada.service.Library.repository;

import edu.ada.service.Library.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserEntity,Long> {

    UserEntity findFirstByEmailAndPassword(String email,String password);

    UserEntity findFirstByEmail(String email);

    UserEntity findFirstByToken(String token);
}

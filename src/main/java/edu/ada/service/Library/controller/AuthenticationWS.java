package edu.ada.service.Library.controller;

import edu.ada.service.Library.model.dto.RegistrationModel;
import org.springframework.http.ResponseEntity;

public interface AuthenticationWS {
    ResponseEntity login(String email,String password);

    ResponseEntity register(RegistrationModel formDATA);
}

package edu.ada.service.Library.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.ada.service.Library.model.dto.BookModel;

import javax.persistence.*;
import java.util.List;

@Entity(name="BookEntity")
@Table(name="Books")
public class BookEntity {

    @Id
    @SequenceGenerator(
            name = "book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "book_sequence"
    )

    private Long bookId;

    @Column(
            unique = true
    )
    private String bookName;
    private String authorName;
    private String authorSurname;
    private String category;
    private Boolean available;

    @OneToMany(targetEntity = AddedBooksEntity.class, mappedBy = "added", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AddedBooksEntity> addedBooks;

    public BookEntity() {
    }

    public BookEntity(BookModel bookModel) {
        this.bookName = bookModel.getBookName();
        this.authorName = bookModel.getAuthorName();
        this.authorSurname = bookModel.getAuthorSurname();
        this.category = bookModel.getCategory();
        this.available = bookModel.getAvailable();
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}

package edu.ada.service.Library.model.dto;


import java.io.Serializable;


public class BookModel implements Serializable {


    private String bookName;
    private String authorName;
    private String authorSurname;
    private String category;
    private Boolean available;

    public BookModel(String s){

    }

    public BookModel(String bookName, String authorName, String authorSurname, String category,Boolean available) {
        this.bookName = bookName;
        this.authorName = authorName;
        this.authorSurname = authorSurname;
        this.category = category;
        this.available=available;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "bookName='" + bookName + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorSurname='" + authorSurname + '\'' +
                ", category='" + category + '\'' +
                ", available=" + available +
                '}';
    }
}
